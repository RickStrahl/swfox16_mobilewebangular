﻿# Creating Mobile Applications with Angular Js and Web Connection

### Session notes, Slides and samples from the Southwest Fox 2016 session.
These are the Southwest Fox 2016 support files for the above session. There's lots of material with a large file of session notes, slides and several examples.


* [Session Notes](https://bitbucket.org/RickStrahl/swfox16_mobilewebangular/raw/2ac90eefd2762f30f8a1e1554b7f85d14ec39536/Documents/Strahl_MobileWebAngular2.pdf)
* [Session Slides](https://bitbucket.org/RickStrahl/swfox16_mobilewebangular/raw/2ac90eefd2762f30f8a1e1554b7f85d14ec39536/Documents/Strahl_MobileWebAngular2.pptx)

### AlbumViewer Angular 2.0 Sample:

* [AlbumViewer Live Site](https://albumviewer2swf.west-wind.com) 
* [Source Code for Sample](https://bitbucket.org/RickStrahl/southwestfoxalbumviewer2)

### TimeTrakker Server Side Example:

* [Time Trakker Live Site](http://timetrakkerswf.west-wind.com/)
* [Source Code for Sample](https://bitbucket.org/RickStrahl/southwestfoxtimetrakker)

### GeoCrumbs Angular 1.0:

* [GeoCrumbs Web Site](https://geocrumbs.net/)
