using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Westwind.Utilities;

namespace GeoCrumbs.Business
{
	public class User
	{
	    public User()
	    {
	        Id = AppUtils.GenerateId();
	        Locations = new List<Location>();
            Entered = DateTime.UtcNow;
            Updated = Entered;
		}
        
		public string Id { get; set; }
                
        public string UserName { get; set; }
        
        public string Password { get; set; }
       
        public string Alias { get; set; }
        
        public string Email { get; set; }
	    
        public bool IsAdmin { get; set; }
        public bool UseGoogleMaps { get; set; }

        public List<Location> Locations { get; set;  }

        public DateTime Entered { get; set; }
        public DateTime Updated { get; set; }       
	}
}

