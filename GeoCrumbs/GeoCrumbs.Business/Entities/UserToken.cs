using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Westwind.Utilities;

namespace GeoCrumbs.Business
{
    public class UserToken
    {
        public UserToken()
        {
            Id = AppUtils.GenerateId();
            Entered = DateTime.UtcNow;
        }
        
        public string Id { get; set; }
        public DateTime Entered { get; set;  }               
        public string UserId { get; set; }        
    }
}