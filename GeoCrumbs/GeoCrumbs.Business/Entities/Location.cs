﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Westwind.Utilities;

namespace GeoCrumbs.Business
{
    [DebuggerDisplay("{Name}")]
    public class Location 
    {
        public Location()
        {
            Id = AppUtils.GenerateId();
            Entered = DateTime.UtcNow;
            LastSync = Entered;
            Usage = Usages.OneTime;            
        }

        public string Id { get; set; }

        [StringLength(150)]
        public string  Name { get; set; }
        public string Address { get; set;  }
        public string  Notes { get; set; }

        public double Longitude { get; set; }
        public double Latitude { get; set; }        

        public Usages Usage { get; set; }

        public bool Deleted { get; set; }

        public DateTime? Entered { get; set;  }
        public DateTime? Updated { get; set;  }
        public DateTime? LastSync { get; set; }

    }

    public class LocationComparer : IEqualityComparer<Location>
    {
        public bool Equals(Location x, Location y)
        {
            if (x.Id != y.Id)
                return false;

            if (x.LastSync < y.LastSync)
                return false;

            if (x.Updated < y.Updated)
                return false;

            return true;
        }

        public int GetHashCode(Location obj)
        {
            return (obj.Id + obj.Updated).GetHashCode();
        }
    }

    public enum Usages
    {
        Frequent,
        Repeated,
        OneTime,
        Rarely,
        Favorite
    }
}
