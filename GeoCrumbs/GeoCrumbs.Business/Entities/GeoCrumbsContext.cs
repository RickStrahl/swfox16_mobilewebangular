﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Westwind.Data.MongoDb;

namespace GeoCrumbs.Business
{
    public class GeoCrumbsContext : MongoDbContext
    {
        
    }

    //public class GeoCrumbsContext : EfCodeFirstContext
    //{

    //    public GeoCrumbsContext()
    //    {
    //        // don't check for schema changes and conflicts
    //        //Database.SetInitializer<BreadCrumbsContext>(null);

    //        //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MarvelPressWorkflowContext, MarvelPress.Workflow.Business.Migrations.Configuration>());
    //        //Database.SetInitializer(new  BreadCrumbsContextDbInitializer());
    //    }

    //    public DbSet<Location> Locations { get; set; }
    //    public DbSet<User> Users { get; set; }
    //    public DbSet<UserToken> UserTokens { get; set; }


    //    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //    {
    //        base.OnModelCreating(modelBuilder);

    //    }
    //}


    //public class BreadCrumbsContextDbInitializer : DropCreateDatabaseIfModelChanges<GeoCrumbsContext>
    //    //DropCreateDatabaseAlways<BreadCrumbsContext>
    //    //DropCreateDatabaseIfModelChanges<TimeTrakkerContext>
    //{

    //    protected override void Seed(GeoCrumbsContext context)
    //    {
    //        // Set extra integrity rules 
    //        //SetDeleteAndUpdateRules(context);

    //        // Seed initial data
    //        SeedInitialData(context);
    //    }

    //    private void SeedInitialData(GeoCrumbsContext context)
    //    {
    //        var user = new User()
    //        {
    //            Email = "rstrahl@west-wind.com",
    //            Alias = "Rick Strahl",                
    //            UserName = "rstrahl@west-wind.com"
    //        };
    //        user.Password = AppUtils.EncodePassword("password", user.Id);
    //        context.Users.Add(user);

    //        user = new User()
    //        {
    //            Email = "megger@eps-software.com",
    //            Alias = "Markus Egger",                
    //            UserName = "megger@eps-software.com"
    //        };
    //        user.Password = AppUtils.EncodePassword("password", user.Id);
    //        context.Users.Add(user);

    //        context.SaveChanges();
    //    }
    //}
}