﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Westwind.Utilities.InternetTools;
using System.Web;
using System.IO;
using System.Net.Mail;
using Westwind.Utilities;

namespace GeoCrumbs.Business
{
    /// <summary>
    /// General application level utilities
    /// </summary>
    public class AppUtils
    {
        //public static object _SyncLock = new object();

        /// <summary>
        /// Global routine used to create unique IDs for users
        /// </summary>
        /// <returns></returns>
        public static string GenerateId()
        {
            return DataUtils.GenerateUniqueId(15);
        }

        /// <summary>
        /// Returns an MD5 hashed and salted password.
        /// 
        /// Encoded Passwords end in || to indicate that they are 
        /// encoded so that bus objects can validate values.
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string EncodePassword(string password, string uniqueSalt)
        {
            // don't allow empty password
            if (string.IsNullOrEmpty(password))
                return string.Empty;

            string s2 = "32510";
            string s3 = uniqueSalt + password + s2;

            var sha = new SHA256CryptoServiceProvider();
            byte[] Hash = sha.ComputeHash(Encoding.ASCII.GetBytes(s3));            

            var md5 = new SHA1CryptoServiceProvider();
            Hash = md5.ComputeHash(Hash);

            return Convert.ToBase64String(Hash).Replace("==", "") +
                // add a marker so we know whether a password is encoded 
                App.PasswordEncodingPostfix;
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="recipients"></param>
        /// <param name="sendAsHtml"></param>
        /// <param name="sendAsync"></param>
        /// <returns></returns>
        public static bool SendEmail(string title, string message, string senderEmail, string recipients, bool sendAsHtml = false, bool sendAsync = false)
        {
            string error = null;
            return SendEmail(title, message, senderEmail, recipients, out error, sendAsHtml, sendAsync);
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="recipients"></param>
        /// <param name="sendAsHtml"></param>
        /// <param name="sendAsync"></param>
        /// <returns></returns>
        public static bool SendEmail(string title, string message, string senderEmail, string recipients, out string errorMessage, bool sendAsHtml = false, bool sendAsync = false)
        {
            errorMessage = null;

            SmtpClientNative smtp = new SmtpClientNative();
            smtp.MailServer = App.Configuration.MailServer;
            smtp.Username = App.Configuration.MailServerUserName;
            smtp.Password = App.Configuration.MailServerPassword;
            smtp.UseSsl = App.Configuration.MailServerUseSsl;
            

            if (string.IsNullOrEmpty(senderEmail))
                senderEmail = App.Configuration.AdminSenderEmail;

            smtp.SenderEmail = senderEmail;

            smtp.Recipient = recipients;
            smtp.Subject = title;
            smtp.Message = message;


            if (sendAsHtml)
            {
                smtp.ContentType = "text/html";
            }

            if (sendAsync)
            {
                smtp.SendMailAsync();
                return true;
            }

            bool result = smtp.SendMail();
            if (!result)
                errorMessage = smtp.ErrorMessage;

            return result;
        }

        /// <summary>
        /// Sends an administrative email
        /// </summary>
        /// <param name="title">Subject line for the email</param>
        /// <param name="message">Contet for the email</param>
        /// <param name="recipients">Optional - Comma Recipient list. If not passed goes to admin email</param>
        /// <param name="sendAsHtml"></param>
        /// <param name="sendAsync"></param>
        /// <returns></returns>
        public static bool SendAdminEmail(string title, string message, string recipients = null, bool sendAsHtml = false, bool sendAsync = false)
        {
            if (recipients == null)
                recipients = App.Configuration.AdminEmailRecipient;

            return SendEmail(title, message,
                            App.Configuration.AdminSenderName + " <" + App.Configuration.AdminSenderEmail + ">",
                            recipients, sendAsHtml, sendAsync);
        }


    }
}
