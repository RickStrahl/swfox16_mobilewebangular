﻿using Westwind.Utilities.Configuration;

namespace GeoCrumbs.Business
{
    public class GeoCrumbsConfiguration : AppConfiguration
    {
        public string ApplicationName { get; set; }

        public string MailServer { get; set; }
        public string MailServerUserName { get; set; }
        public string MailServerPassword { get; set; }
        public bool MailServerUseSsl { get; set; }

        public string GooglePlacesApiKey { get; set; }

        public string AdminEmailRecipient { get; set; }
        public string AdminSenderEmail { get; set; }
        public string AdminSenderName { get; set; }
        
        public GeoCrumbsConfiguration()
        {
            ApplicationName = "GeoCrumbs";            
        }
    }
}