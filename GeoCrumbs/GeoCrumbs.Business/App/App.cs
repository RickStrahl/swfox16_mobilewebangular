﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoCrumbs.Business
{
    public class App
    {
        public static DateTime EMPTY_DATE;
        
        public static GeoCrumbsConfiguration Configuration { get; set; }

        public static string PasswordEncodingPostfix = "|~";

        static App()
        {
            EMPTY_DATE = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            Configuration = new GeoCrumbsConfiguration();
            Configuration.Initialize();
        }
    }
}
