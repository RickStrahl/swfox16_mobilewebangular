﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoCrumbs.Business
{
    public class LocationsSync
    {
        public string userToken { get; set; }
        public DateTime lastSync { get; set; }
        public Location[] history { get; set; }
    }


    //public class LocationObject
    //{
    //    public string Id { get; set; }
    //    public string Name { get; set; }
    //    public string Address { get; set; }
    //    public Usages Usage { get; set; }
    //    public string Notes { get; set; }
    //    public double? Longitude { get; set; }
    //    public double? Latitude { get; set; }
    //    public DateTime? Entered { get; set; }
    //    public DateTime? LastSync { get; set;  }
    //}
}
