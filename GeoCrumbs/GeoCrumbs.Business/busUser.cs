﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using Westwind.Data.MongoDb;
using Westwind.Utilities;

namespace GeoCrumbs.Business
{
    public class busUser : MongoDbBusinessBase<User, GeoCrumbsContext>
    {
        public busUser()
        {
            AutoValidate = true;
        }

        /// <summary>
        /// Authenticates a user based on email and password
        /// </summary>
        /// <param name="username">username or email address</param>
        /// <param name="password">password for user</param>
        /// <returns>User record or null on failure to authenticate</returns>
        public User AuthenticateAndLoad(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                SetError("Email address can't be empty");
                return null;
            }

            if (string.IsNullOrEmpty(password) || password.Length < 5)
            {
                SetError("Missing password or too short");
                return null;
            }

            username = username.Trim();
            password = password.Trim();

            var q = Query<User>.EQ(usr => usr.Email, username);
            User user = Collection.FindOne(q);
            if (user == null)
            {
                SetError("Invalid credentials.");
                return null;
            }

            // Password is compared as HashCode only
            string encodedPassword = AppUtils.EncodePassword(password, user.Id);
            if (encodedPassword != user.Password)
            {
                SetError("Invalid credentials.");
                return null;
            }

            return user;
        }

        /// <summary>
        /// Authenticates a user based on email and password
        /// </summary>
        /// <param name="username">username or email address</param>
        /// <param name="password">password for user</param>
        /// <returns>true or false based on match</returns>
        public bool Authenticate(string username, string password)
        {
            var user = AuthenticateAndLoad(username, password);
            if (user == null)
                return false;

            return true;
        }

        public User LoadFromEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return null;

            return LoadBase(usr => usr.Email == email);
        }        

        public bool RecoverPassword(string username)
        {
            var user = LoadBase(usr => usr.Email == username);
            if (user == null)
            {
                SetError("Invalid user name.");
                return false;
            }

            string password = DataUtils.GenerateUniqueId(8);

            user.Password = AppUtils.EncodePassword(password, user.Id);
            if (!Save(user))
                return false;

            AppUtils.SendEmail(App.Configuration.ApplicationName + " Password Recovery",
                "This message contains a temporary password so you can reset your password " +
                "Please use this temporary password to login, then access your account profile and change " +
                "it to something you can remember.\r\n\r\n" +
                "Your temporary password is: " + password + "\r\n\r\n" +
                "The GeoCrumbs Team",
                App.Configuration.AdminSenderEmail,
                user.Email);

            return true;
        }


        /// <summary>
        /// Returns a list of recent locations for the user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<Location> GetRecentLocations(string userId, int count = 300)
        {
            var user = Load(userId);
            if (user == null)
                return null;

            return user.Locations           
                .Where(loc=> !loc.Deleted)
                .OrderByDescending(loc => loc.Entered)
                .Take(count);
        }


        /// <summary>
        /// Merges locations into a single list that is then returned.
        /// </summary>
        /// <param name="locations">The client list of locations</param>
        /// <param name="userToken">Usertoken for the user accessing the locations</param>
        /// <returns>Merged locations or null on failure</returns>
        public IEnumerable<Location> SyncLocations(IEnumerable<Location> locations, string userToken)
        {
            busUser userBus = new busUser();
            var user = userBus.TokenToUser(userToken);

            // just return what you got
            if (user == null)
            {
                SetError("Invalid user id or not logged in.");
                return null;
            }


            foreach (var location in locations)
            {
                var existingLoc = user.Locations
                                      .FirstOrDefault(loc => loc.Id == location.Id || 
                                                     (loc.Latitude == location.Latitude &&
                                                     loc.Longitude == location.Longitude));

                if (location.Name.StartsWith("Makawao"))                
                {
                    int x = 0;
                    x++;
                }
                if (existingLoc != null)
                {
                    if (existingLoc.Deleted)
                        continue;

                    if (location.Deleted)
                    {
                        existingLoc.Deleted = true;
                        continue;
                    }

                    // record exists update it
                    if (existingLoc.Updated >= location.Updated)
                        continue;

                    

                    DataUtils.CopyObjectData(location, existingLoc, "Id,Updated");

                    existingLoc.LastSync = DateTime.UtcNow;
                    existingLoc.Updated = DateTime.UtcNow;
                }
                else
                {
                    // new record
                    location.LastSync = DateTime.UtcNow;                        
                    user.Locations.Insert(0, location);
                }
            }

            // remove duplicates
            user.Locations =                
                user.Locations                    
                    .OrderByDescending(loc => loc.Updated)
                    .GroupBy(loc => new {loc.Latitude, loc.Longitude})
                    .Select(loc => loc.First())
                    .ToList();                          

            if (!Save(user))
                return null;

            return user.Locations.Where(loc => !loc.Deleted);
        }


        /// <summary>
        /// Overridden to encode the password
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override bool OnBeforeSave(User entity)
        {
            // do nothing - but OK
            if (entity == null)
                return true;

            // if new or changed password encode it
            if (string.IsNullOrEmpty(entity.Id) ||
               (!string.IsNullOrEmpty(entity.Password) && !entity.Password.EndsWith(App.PasswordEncodingPostfix)))
            {
                // Password needs to be written to the database encoded
                entity.Password = AppUtils.EncodePassword(entity.Password,entity.Id);
            }            

            return true;
        }

        /// <summary>
        /// Creates a new user token that can be passed back to the user and reused for subsequent
        /// requests.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string CreateToken(string userId = null)
        {
            if (userId == null)
                userId = Entity.Id;

            var token = new UserToken();
            token.UserId = userId;

            if (!Save<UserToken>(token))
            {
                SetError("Unable to save user token.");
                return null;
            }

            // delete expired tokens
            var q = Query<UserToken>.LT<DateTime>(usr => usr.Entered, DateTime.UtcNow.AddDays(-5));
            var result = Database.GetCollection<UserToken>("UserToken").Remove(q);

            if (result.HasLastErrorMessage)
            {
                SetError(result.LastErrorMessage);
                return null;
            }
            
            return token.Id;
        }

        /// <summary>
        /// Returns a userId from a user token.
        /// If not found null is returned and 
        /// calling code needs to check for null.
        /// </summary>
        /// <param name="userToken">User token to look up</param>
        /// <returns>user id or null if not found</returns>
        public string TokenToUserId(string userToken)
        {
            var q = Query<User>.EQ(tok => tok.Id, userToken);            
            var token = Database.GetCollection<UserToken>("UserTokens").FindOne(q);

            if (token == null)
                return null;

            return token.UserId;
        }

        /// <summary>
        /// Returns a user object from a user token
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public User TokenToUser(string userToken)
        {
            var userId = TokenToUserId(userToken);
            if (userId == null)
                return null;

            return Load(userId);
        }


        protected override void OnValidate(User entity)
        {
            base.OnValidate(entity);

            if (!Collection.AsQueryable().Any(usr => usr.Id == entity.Id))
            {
                if (Collection.AsQueryable()
                             .Any(usr => usr.UserName == entity.UserName || usr.Email == entity.Email))
                    ValidationErrors.Add("Username/Email exists already.", "Email");
            }          
        }
    }
}
