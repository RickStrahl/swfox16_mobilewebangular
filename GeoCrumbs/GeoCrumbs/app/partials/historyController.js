﻿(function() {
    'use strict';

    // define controller
    angular
      .module('app')
      .controller('historyController', historyController);

    // dependencies
    historyController.inject = ["$location", "$http", "$timeout", "$routeParams", "locationData"];

    // implementation
    function historyController($location, $http, $timeout, $routeParams, locationData) {
        console.log("HistoryController called.");

        // controller as view model
        var vm = this;

        // properties
        vm.locationHistory = locationData.locationHistory;
        vm.historySearch = null;
        vm.numberOfItems = locationData.locationHistory.length;

        // methods
        vm.showOptions = showOptions;
        vm.showOnMap = showOnMap;
        vm.showDirections = showDirections;
        vm.editItem = editItem;
        vm.deleteItem = deleteItem;
        vm.backToMap = backToMap;        

        // Initialization 
        initialize();                
        return;

        function initialize() {
            bc.showMap(true, "#HistoryIcon");
        }

        function showOptions($event) {
            var $el = $($event.target);
            $el = $el.parents(".history-item");

            //var $opt = $("#History #HistoryItemOptions");
            var $opt = $("#SlideWrapper");

            if (!$opt.hasClass("height-transition-hidden"))
                $opt.slideUpTransition();
            else {
                $opt.insertAfter($el);

                // have to delay for element to register in place
                setTimeout(function() {
                    $opt.slideDownTransition();
                }, 20);

                // if bottom item make it visible 
                var rect = $opt[0].getBoundingClientRect();
                var offset = $(window).height() - rect.bottom;
                if (offset < 90) {
                    var area = $(".content-area:visible");
                    area.scrollTop(area.scrollTop() + 90 - offset);
                }
            }
        }

        function showOnMap($event) {
            var id = getLocationIdFromButton($event);
            bc.showHistoryItemOnMap(id);
            $location.path("/map");
        };

        function showDirections($event) {
            var id = getLocationIdFromButton($event);
            bc.showHistoryDirections(id);
        };

        function editItem($event) {
            var id = getLocationIdFromButton($event);
            if (!id)
                return;
            $location.path("/saveLocation/" + id);
        };

        function deleteItem($event) {
            var id = getLocationIdFromButton($event);

            bc.deleteHistoryItem(id);
            locationData.saveLocationData();

            $timeout(function() {
                var $el = $($event.target);
                var $item = $el.parents("#SlideWrapper").prev();

                $("#SlideWrapper")
                    .appendTo("#History");

                $item.slideUp(800, function() {
                    vm.locationHistory = locationData.locationHistory;
                });
            }, 20);

            // call and forget
            $http.put("../api/locations/delete/" + id, {
                userToken: locationData.userToken
            });
        };

        function backToMap() {
            $location.path("/map");
        };

        function getLocationIdFromButton($event) {
            var $el = $($event.target);
            $el = $el.parents("#HistoryItemOptions").parent().prev();
            return $el.data("id");
        }
    }
})();