﻿app.controller('saveLocationController', ["$scope", "$location", "$routeParams", "locationData",
    function($scope, $location, $routeParams, locationData) {
        console.log("SaveLocationController");        

        $scope.saveLocation = function (location) {
            console.log(location,$scope.locationData);
            
            location.Updated = new Date();
            locationData.saveLocationItem(location);

            // clear out the location data
            locationData.location = locationData.createLocationData();
            bc.showMap();
            
            $location.path("/history");
        };
        $scope.cancel = function () {
            if (!$routeParams.id)
                $location.path("/map");
            else
                $location.path("/history");
        };
        $scope.clearName = function() {
            $scope.locationData.Name = "";
            $("#SaveLocationDialog #txtName").focus();
        };

        $scope.locationHistory = locationData.locationHistory;
        if ($routeParams.id) {
            locationData.location = bc.findHistoryItem($routeParams.id);            
            if (locationData.location)
                locationData.location.IsNew = false;
        }
        if (!locationData.location)
            locationData.location = locationData.createLocationData();

        $scope.locationData = locationData.location;
       
        var $name = $("#txtName");
        $name.focus();
        $name[0].scrollIntoView(true);
       
        bc.showMap(true);

        // Windows Phone doesn't work with AutoComplete
        if (!app.isWindowsPhone()) {
            var autocomplete = new google.maps.places.Autocomplete($name[0]);
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                setTimeout(function() {
                    $scope.locationData.Name = place.name;
                    $name.val(place.name);
                }, 50);
            });            
        }

    }]);