﻿app.controller('settingsController', ["$scope", "$http", "locationData",
    function($scope, $http, locationData) {                
        $scope.locationData = locationData;
        $scope.message = "";
        $scope.syncing = false;
        $scope.icon = "fa fa-ok-sign";
        
        $scope.deleteAllLocations = function () {
            if (!confirm("Are you sure you want to delete all local GeoCrumbs history?"))
                return;
            
            locationData.locationHistory = [];
            locationData.saveLocationData();
            $scope.message = "All locations deleted.";
        };
        

        $scope.syncWithServer = function () {
            $scope.syncing = true;
            if (!locationData.userToken) {
                location.href = "#/login";
                return;
            }

            locationData.syncWithServer()
                .success(function (history) {                    
                    $scope.message = "Data synced.";
                    $scope.icon = "fa fa-info-circle";                    
                })
                .error(function(error, httpCode) {
                    if (httpCode === 401) {
                        location.href = "#/login";
                        return;
                    }
                    $scope.message = "Error syncing: " + error.message;
                    $scope.icon = "fa fa-warning-circle";
                })
                .finally(function() {                   
                    $scope.syncing = false;
                });
            ;
        };
        
        $scope.logout = function () {
            locationData.userToken = "";
            locationData.saveLocationData();
            $scope.message = "Logged out.";
        };

        $scope.backToMap = function () {            
            location.href = "#/map";
        };

        // hide the map        
        bc.showMap(true, "#SettingsIcon");
    }]);


