'use strict';

console.log("app.js launched");

// Declare app level module which depends on filters, and services
window.app = angular.module('app', [
    'ngRoute',
    'ngAnimate',
    'ngTouch',
    'app.filters',
    'app.services',
    'app.directives',
    'app.controllers'
])
  .config(['$routeProvider', function($routeProvider) {
      console.log("app.js config launched");

      $routeProvider.when('/map/:mode',
        {
            template: " ", // just fire controller
            controller: 'mapController',
            reloadOnSearch: false,  
            animation: 'slide'
        });
      $routeProvider.when('/map',
          {
              template: " ", // just fire controller
              controller: 'mapController',              
              animation: 'slide'
          });
      $routeProvider.when('/history', {
          templateUrl: 'partials/history.html',          
          animation: 'slide'          
      });
      $routeProvider.when('/saveLocation', {
          templateUrl: 'partials/savelocation.html',
          controller: 'saveLocationController',
          animation: 'slide'
      });
      $routeProvider.when('/saveLocation/:id', {
          templateUrl: 'partials/savelocation.html',
          controller: 'saveLocationController',
          animation: 'slide'
      });
      $routeProvider.when('/search', {
          templateUrl: 'partials/search.html',
          controller: 'searchController',
          animation: 'slide'
      });
      $routeProvider.when('/settings', {
          templateUrl: 'partials/settings.html',
          controller: 'settingsController',
          animation: 'slide'
      });
      $routeProvider.when('/login', {
          templateUrl: 'partials/login.html',
          controller: 'loginController',
          animation: 'slide'
      });
      $routeProvider.when('/account/:mode', {
          templateUrl: 'partials/account.html',
          controller: 'accountController',
          animation: 'slide'
      });
      $routeProvider.when('/account', {
          templateUrl: 'partials/account.html',
          controller: 'accountController',
          animation: 'slide'
      });
      $routeProvider.when('/landing', {
          templateUrl: 'partials/landing.html',
          controller: 'landingController',
          animation: 'slide'
      });

      // *** PARTIALS

    $routeProvider.otherwise({redirectTo: '/landing'});
  }]);

angular.module('app.controllers', []);
angular.module('app.services', []);
angular.module('app.directives', []);
angular.module('app.filters', [])
  .value('version', '0.1');

app.run(function ($rootScope, $location, $timeout) {
    // Google Analytics
    $rootScope.$on('$routeChangeSuccess', function (route,routeData) {        
        window.ga('send', 'pageview', $location.path());
        
        // always force the document to scroll to top
        // especially for input fields when the keyboard pops up
        // on auto-focus
        //$("body").scrollTop(0);       
    });

    $("body")
           .on("click","a,form", function () {
               var href = $(this).attr("href");
               if (href) {
                   window.location = href;
                   return false;
               }
               return true;
           });
});

app.getActiveScope = function() {
    return angular.element($("#ViewPlaceholder")).scope();
};
app.gravatarLink = function (email, size) {    
    var hash = md5(email);
    if (!size)
        size = 80;
    var url = "http://www.gravatar.com/avatar/" + hash + "?s=" + size + "&r=r";
    return url;
};
app.isWindowsPhone = function() {
    return window.navigator.userAgent.indexOf("Windows Phone") > 0;
};

