(function() {
    'use strict';

    var app = angular
        .module('app');

    app.factory("locationData", locationData);
    locationData.inject = ["$http"];

    function locationData($http) {
        console.log("locationData Service loaded");

        var root = {};

        root.createUniqueId = function() {
            // creates a random string of 4 characters

            function Random4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            // string together 4 random hex4 values
            return Random4() + Random4() + Random4() + Random4();
        };
        root.createLocationData = function() {
            return {
                Id: root.createUniqueId(),
                Name: "",
                Address: "",
                Usage: "OneTime",
                Notes: "",
                Longitude: 0,
                Latitude: 0,
                Entered: new Date(),
                Updated: new Date(),
                LastSync: new Date(),
                IsNew: true
            };
        };
        root.createCredentialsData = function() {
            return {
                username: null,
                password: null,
                name: null,
                useGoogleMaps: false,
                id: null

            };
        };


        root.saveLocationItem = function(loc) {
            if (!loc)
                loc = root.location;

            loc.Updated = new Date();

            if (loc.IsNew) {
                loc.Entered = new Date();
                root.locationHistory.splice(0, 0, loc);
            }
            // otherwise the item is just updated in the array

            // MUST clear the reference or else everything
            //      in the array points at the same object!
            root.location = root.createLocationData();

            root.saveLocationData();
        };


        root.saveLocationData = function() {
            if (root.locationHistory == null)
                root.locationHistory = [];

            if (root.locationHistory.length > 100)
                root.locationHistory.splice(100, root.locationHistory.length - 100);

            localStorage.setItem("locationHistory", JSON.stringify(root.locationHistory));
            localStorage.setItem("locationToken", root.userToken ? root.userToken : "");
            if (!root.userToken)
                root.userEmail = "";
            localStorage.setItem("locationEmail", root.userEmail ? root.userEmail : "");
            localStorage.setItem("locationUseGoogleMaps", root.useGoogleMaps ? root.useGoogleMaps : false);
        };

        root.loadLocationData = function($scope) {
            if (root.locationHistory && root.locationHistory.length > 0) {
                return [];
            }

            var locationHistory = localStorage.getItem("locationHistory");

            if (!locationHistory) {
                return [];
            }
            var data = JSON.parseWithDate(locationHistory);
            root.userToken = localStorage.getItem("locationToken");
            if (root.userToken)
                root.userEmail = localStorage.getItem("locationEmail");
            else
                root.userEmail = "";

            root.useGoogleMaps = localStorage.getItem("locationUseGoogleMaps");
            root.useGoogleMaps = root.useGoogleMaps == "true" ? true : false;

            if ($scope)
                $scope.$apply();

            return data;
        };

        root.syncWithServer = function() {
            return $http.post("../api/locations/sync",
                {
                    userToken: root.userToken,
                    lastSync: root.lastSync,
                    history: root.locationHistory
                })
                .success(function (history) {                    
                    // fix up dates
                    _.each(history, function(loc) {
                        loc.Entered = JSON.dateStringToDate(loc.Entered, new Date());
                        loc.Updated = JSON.dateStringToDate(loc.Updated, new Date());
                        loc.LastSync = new Date();
                    });
                    root.locationHistory = history;
                    root.lastSync = new Date();
                    root.saveLocationData();
                });

        };


        // single location
        root.location = root.createLocationData();
        root.userToken = null;
        root.userEmail = null;
        root.useGoogleMaps = false;
        root.mapMessage = "";
        root.searchAddress = null;
        root.searchErrorMessage = null;

        // a full history
        root.locationHistory = root.loadLocationData();

        root.lastSync = new Date(2010, 0, 1);

        bc.$scope = root;

        return root;     
    };
})();